import { CardInfoResponse } from "./cardInfo.response";
import {
    CardStateResponse,
    CardValidityResponse,
} from "../../../../dataProviders/card/card.response";
import { formatDateISO8601ToEuropeDate } from "../../../../helpers/datetime.helper";

export const mapCardInfoResponse = (
    cardStatus: CardStateResponse,
    cardValidity: CardValidityResponse,
): CardInfoResponse => ({
    valid_to: formatDateISO8601ToEuropeDate(cardValidity.validity_end),
    state: cardStatus.state_description,
});
