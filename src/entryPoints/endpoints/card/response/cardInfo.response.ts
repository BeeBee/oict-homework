export interface CardInfoResponse {
    valid_to: string;
    state: string;
}
