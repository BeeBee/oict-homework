import { Router } from "express";
import { Service } from "typedi";

import { CardController } from "./card.controller";
import { CardNumberParameter } from "./request/request.parameters";
import { ApiKeyMiddleware } from "../../middlewares/apiKey.middleware";

@Service()
export class CardRouter {
    private readonly router: Router = Router();

    public constructor(
        private readonly cardController: CardController,
        private readonly apiKeyMiddleware: ApiKeyMiddleware,
    ) {
        this.router.use(apiKeyMiddleware.handle);

        this.router.get<CardNumberParameter>(
            "/card/:cardNumber",
            cardController.getCardInfoHandler,
        );
    }

    public getRouter() {
        return this.router;
    }
}
