import { Application } from "express";
import swaggerUi from "swagger-ui-express";
import { Service } from "typedi";

import openApi3docs from "../../../docs/open-api-v3.json";
import { CardRouter } from "../endpoints/card/card.router";
import { StatusRouter } from "../endpoints/status/status.router";

@Service()
export class MainRouter {
    constructor(
        private readonly statusRouter: StatusRouter,
        private readonly cardRouter: CardRouter,
    ) {}

    public applyRoutes(app: Application): Application {
        app.use("/swagger", swaggerUi.serve, swaggerUi.setup(openApi3docs));

        app.use(this.statusRouter.getRouter());
        app.use(this.cardRouter.getRouter());

        return app;
    }
}
