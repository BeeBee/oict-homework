import { expect } from "chai";
import { describe } from "mocha";

import { formatDateISO8601ToEuropeDate } from "../../src/helpers/datetime.helper";

describe("Datetime helpers", () => {
    const testCases = [
        { input: "2016-08-12T00:00:00", expected: "12.8.2016" },
        { input: "2019-01-10T00:00:00", expected: "10.1.2019" },
        { input: "2024-12-01T10:00:00", expected: "1.12.2024" },
    ];

    testCases.forEach(function (testCase) {
        it(`should convert ISO8601 date ${testCase.input} to European common ${testCase.expected}`, function () {
            const result = formatDateISO8601ToEuropeDate(testCase.input);

            expect(result).to.equal(testCase.expected);
        });
    });

    it("should throw error on wrong date format", () => {
        const wrongDate = "blabol";

        const formatError = () => formatDateISO8601ToEuropeDate(wrongDate);

        expect(formatError).to.throw("Wrong input date");
    });
});
